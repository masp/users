package users

import (
	"strings"

	"github.com/gin-gonic/gin"
)

// GetMailFromContext liefert die Mail des angemeldeten Nutzers zurück
func GetMailFromContext(c *gin.Context) string {
	l := m.Log("Mail from Context")
	hash := c.GetHeader("Authorization")
	if !strings.HasPrefix(hash, "Bearer") {
		l.Info("Not Bearer token")
		return ""
	}
	hash = strings.TrimPrefix(hash, "Bearer ")
	if hash == "" {
		l.Debug("Not logged in")
		return ""
	}
	return hashes[hash]
}

// GetUserIDFromContext liefert die Nutzer ID des angemeldeten Nutzers zurück
func GetUserIDFromContext(c *gin.Context) int {
	return GetUserIDFromMail(GetMailFromContext(c))
}

// GetUserIDFromMail gibt für die angegebene Mail die Nutzer ID zurück, Wenn der Nutzer nicht existiert, wird 0 zurück gegeben
func GetUserIDFromMail(mail string) int {
	l := m.Log("UserID From Mail")
	if mail == "" {
		l.Error("Empty mail")
		return 0
	}
	l.Debug("for User %s", mail)
	id := 0
	err := m.GetDatabase().QueryRow("SELECT user_id FROM users_users WHERE mail = ?", mail).Scan(&id)
	if err != nil {
		l.Error("%s", err)
		return 0
	}
	if id == 0 {
		l.Error("No User found")
	}
	return id
}

// GetMailFromUserID gibt die Mail des Nutzers für  die ID zurück
func GetMailFromUserID(id int) string {
	l := m.Log("Mail From UserID")
	if id <= 0 {
		l.Error("Empty UserID")
		return ""
	}
	l.Debug("for User ID %d", id)
	mail := ""
	err := m.GetDatabase().QueryRow("SELECT mail FROM users_users WHERE user_id = ?", id).Scan(&mail)
	if err != nil {
		l.Error("%s", err)
		return ""
	}
	if mail == "" {
		l.Error("No User found")
	}
	return mail
}

//GetNameFromUserID gibt den Anzeigenamen des Nutzer mit der Nutzer ID id zurück
func GetNameFromUserID(id int) string {
	l := m.Log("Name From UserID")
	if id <= 0 {
		l.Error("Empty UserID")
		return ""
	}
	l.Debug("for User ID %d", id)
	name := ""
	err := m.GetDatabase().QueryRow("SELECT name FROM users_users WHERE user_id = ?", id).Scan(&name)
	if err != nil {
		l.Error("%s", err)
		return ""
	}
	if name == "" {
		l.Error("No User found")
	}
	return name
}

// IsAdmin gibt true zurück, wenn der Nutzer mit der id ein Admin ist
func IsAdmin(id int) bool {
	l := m.Log("Is Admin")
	if id <= 0 {
		l.Error("Empty UserID")
		return false
	}
	l.Debug("for User ID %d", id)
	adminFlag := 0
	err := m.GetDatabase().QueryRow("SELECT admin_flag FROM users_users WHERE user_id = ?", id).Scan(&adminFlag)
	if err != nil {
		l.Error("%s", err)
		return false
	}
	if adminFlag == 2 {
		l.Debug("User is Admin")
		return true
	}
	l.Debug("User is not Admin")
	return false
}

//GetUserState überprüft, welche Rechte ein Nutzer hat. Es wird auf den Authorization Header (https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization) zugegriffen.
// Der Header muss den Zusatz Bearer (https://tools.ietf.org/html/rfc6750) haben. Wenn der Header vorhanden ist, wird in der Datenbank nach dem admin_flag geguckt.
// So wird auch überprüft, ob ein Nutzer in der Datenbank noch vorhanden ist.
// Die Funktion gibt zurück, was für eine Nutzerrolle der angemeldete Nutzer hat.
func GetUserState(c *gin.Context) UserRole {
	l := m.Log("User State")
	user := GetMailFromContext(c)
	l.Debug("User: %s", user)
	if user == "" {
		l.Debug("Not logged in")
		return UserIsNotLoggedIn
	}
	var admin int
	m.GetDatabase().QueryRow("Select admin_flag FROM users_users WHERE mail like ?", user).Scan(&admin)
	l.Debug("%d", admin)
	switch admin {
	case 2:
		l.Info("User is admin")
		return UserIsAdmin
	case 1:
		l.Info("Normal user")
		return UserIsLoggedIn
	default:
		// Nutzer ist nicht in Datenbank/ein gelöschter Nutzer, alle Hashes des Nutzer werden gelöscht.
		l.Debug("not in database, but hash exists")
		count := 0
		for key, value := range hashes {
			if value == user {
				count++
				delete(hashes, key)
			}
		}
		l.Debug("%d Keys deleted for %s", count, user)
		return UserIsNotLoggedIn
	}
}

//RequireAdmin prüft, ob der Nutzer ein Admin ist.
// 		Admin: return true
// 		else: return false und gebe einen entsprechenden Status zurück, sodass sich in der Funktion um nichts mehr gekümmert werden muss
func RequireAdmin(c *gin.Context) bool {
	user := GetUserState(c)
	if user == UserIsNotLoggedIn {
		c.String(401, "You are not logged in")
		c.Abort()
		return false
	}
	if user == UserIsLoggedIn {
		c.String(403, "You are not an admin")
		c.Abort()
		return false
	}
	return true
}

//RequireUser prüft ob der Nutzer angemeldet ist
//	Nutzer ist angemeldet: return true
//	Sonst: return true und gebe einen entsprechenden Status zurück, sodass sich um nichts mehr gekümmert werden muss
func RequireUser(c *gin.Context) bool {
	user := GetUserState(c)
	if user == UserIsNotLoggedIn {
		c.String(401, "You are not logged in")
		c.Abort()
		return false
	}
	return true
}
