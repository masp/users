package users

import (
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/pengux/check"
	"golang.org/x/crypto/bcrypt"
)

//ChangeUser ändert, Mail, Name, Passwort und wenn Rechte vorhanden Adminflag für Nutzer
func changeUser(c *gin.Context) {
	if UserIsNotLoggedIn == GetUserState(c) {
		c.String(401, "You are not logged in")
		c.Abort()
		return
	}

	var (
		dataMail               string
		dataName               string
		dataPassword           []byte
		dataAdminFlag          int
		dataRequestorAdminflag int
		dataRequestorPassword  []byte
	)

	log := m.Log("changeUser")
	globalID, _ := strconv.Atoi(c.Param("user_id"))
	requestor := GetUserIDFromContext(c)
	person := User{}
	err := c.BindJSON(&person)
	if err != nil {
		log.Error("Sorry, Error when binding json: %s, for user %d", err, globalID)
		c.AbortWithError(500, err)
		return
	}

	//password validation for the person, that requested a change
	error := m.GetDatabase().QueryRow("SELECT password FROM users_users WHERE user_id = ?", requestor).Scan(&dataRequestorPassword)
	if error != nil {
		log.Error("Error when fetching requestorpassword out of user_users. Error is: %s", error)
		c.AbortWithError(500, error)
		return
	}
	log.Info("fetching personal password from database was successful")

	val := bcrypt.CompareHashAndPassword(dataRequestorPassword, []byte(person.Password))
	if val != nil {
		log.Error("requestorpassword did not match with databasepassword. requestor is: %d", requestor)
		c.String(401, "please enter personal password")
		c.Abort()
		return
	}

	//derzeitige Nutzer Information aus Datenbank ziehen
	error = m.GetDatabase().QueryRow("SELECT name, mail, password, admin_flag FROM users_users WHERE user_id = ?", globalID).Scan(&dataName, &dataMail, &dataPassword, &dataAdminFlag)
	if error != nil {
		log.Error("Error when fetching personaldata out of user_users. Error is: %s", error)
		c.AbortWithError(500, error)
		return
	}

	error = m.GetDatabase().QueryRow("SELECT admin_flag FROM users_users WHERE user_id = ?", requestor).Scan(&dataRequestorAdminflag)
	if error != nil {
		log.Error("Error when fetching admin_flag of person who requested userchange")
		c.AbortWithError(500, error)
	}

	log.Info("fetching personal data from database was successfull")

	//checken ob eigenen Daten oder Daten als Amdin verändert werden sollen
	if globalID == requestor || dataRequestorAdminflag == 2 {

		//aktualisiere Namen in Datenbank
		if person.Name != dataName && person.Name != "" {

			//validiere neuen Username
			err2 := check.MinChar{2}.Validate(person.Name)
			if err2 != nil {
				log.Error("Username %s is too short", person.Name)
				c.String(400, err2.Error())
				return
			}
			err2 = check.NonEmpty{}.Validate(person.Name)
			if err2 != nil {
				log.Error("Username cannot be empty")
				c.String(400, err2.Error())
				return
			}

			log.Info("Validation of new Username successfull. Attempting to change name in database now")

			_, err := m.GetDatabase().Exec("UPDATE users_users SET name = ? WHERE user_id = ?", person.Name, globalID)
			if err != nil {
				log.Error("Username is diffrent from name listed in database, but error when updatding database. Error is: %s", err)
				c.String(500, err.Error())
				return
			}

			log.Info("name change successfull")
		}

		//aktualisiere Mail in Datenbank
		if person.Mail != dataMail && person.Mail != "" {

			//validiere neue Mail
			err2 := check.Email{}.Validate(person.Mail)
			if err2 != nil {
				log.Error("new Mail %s is not valid", person.Mail)
				c.String(400, err2.Error())
				return
			}
			err2 = check.NonEmpty{}.Validate(person.Mail)
			if err2 != nil {
				log.Error("Mail cannot be empty")
				c.String(400, err2.Error())
				return
			}

			log.Info("Validation of new mail successfull. Attempting to change to change mail in database")

			_, err := m.GetDatabase().Exec("UPDATE users_users SET mail = ? WHERE user_id = ?", person.Mail, globalID)
			if err != nil {
				log.Error("Mail is diffrent from listed mail in database, but error when updatding Database. Error is: %s", err)
				c.String(500, err.Error())
				return
			}
			log.Info("mail change successfull")
		}

		//aktualisiere Password
		e := bcrypt.CompareHashAndPassword(dataPassword, []byte(person.NewPassword))

		if e != nil && person.NewPassword != "" {
			//validiere neues Password
			err2 := check.MinChar{2}.Validate(person.NewPassword)
			if err2 != nil {
				log.Error("new Password %s is too short", person.NewPassword)
				c.String(400, err2.Error())
				return
			}
			err2 = check.NonEmpty{}.Validate(person.NewPassword)
			if err2 != nil {
				log.Error("Password cannot be empty, obviously")
				c.String(400, err2.Error())
				return
			}

			log.Info("Validation of new password successfull. Attempting to hash new password")

			hashPassword, hashErr := bcrypt.GenerateFromPassword([]byte(person.NewPassword), bcrypt.DefaultCost)
			if hashErr != nil {
				log.Error("error when hashing new password. Error is: %s", hashErr)
				c.String(500, hashErr.Error())
				return
			}
			log.Info("new password hash created successfully - starting now to change password in database")

			_, err = m.GetDatabase().Exec("UPDATE users_users SET password = ? WHERE user_id = ?", hashPassword, globalID)
			if err != nil {
				log.Error("hashed Password could not be stored in Database. Error is: %s", err)
				c.String(500, err.Error())
				return
			}
			log.Info("password change successfull - starting to log out user from all devices")

			// logging out user
			hash := c.GetHeader("Authorization")
			hash = strings.TrimPrefix(hash, "Bearer ")
			count := 0
			for key, value := range hashes {
				if value == dataMail {
					count++
					delete(hashes, key)
				}
			}
			log.Debug("%d Keys deleted for %s", count, dataName)
			log.Info("successfully logged out")

		}
	} else {
		log.Error("Dont have permission to change Information due to your status: %d, or you trying to change user: %d though you are: %d", dataRequestorAdminflag, globalID, requestor)
		c.String(403, "No Permission")
		c.Abort()
		return
	}

	//aktualisiere Adminflag
	if dataAdminFlag != person.AdminFlag && person.AdminFlag != 0 {
		adminAmount := -1
		error = m.GetDatabase().QueryRow("Select count(admin_flag) FROM users_users WHERE admin_flag = 2").Scan(&adminAmount)
		if error != nil {
			log.Error("Error when fetching amount of admins out of user_users. Error is: %s, adminAmount: %d (-1 when initiated)", error, adminAmount)
			c.AbortWithError(500, error)
			return
		}

		if dataRequestorAdminflag == 2 {

			//validiere Adminflag
			err = check.LowerThan{3}.Validate(person.AdminFlag)
			if err != nil {
				log.Error("Adminflag cannot be greater than 2")
				c.String(400, err.Error())
				return
			}

			err = check.GreaterThan{0}.Validate(person.AdminFlag)
			if err != nil {
				log.Error("Adminflag cannot be lower than 0")
				c.String(400, err.Error())
				return
			}

			log.Info("valdiation of Adminflag successfull. Attempting to update Adminflag in Database")

			if person.AdminFlag == 2 {
				_, err := m.GetDatabase().Exec("UPDATE users_users SET admin_flag = 2 WHERE user_id = ?", globalID)
				if err != nil {
					log.Error("error when updatding Database admin_flag to 2. Error is: %s", err)
				}
				log.Info("successfully created new admin")
			}

			if person.AdminFlag == 1 && adminAmount > 1 {
				_, err := m.GetDatabase().Exec("UPDATE users_users SET admin_flag = 1 WHERE user_id = ?", globalID)
				if err != nil {
					log.Error("error when updatding Database admin_flag to 1. Error is: %s", err)
				}
				log.Info("successfully downgraded admin")

			}

			if person.AdminFlag == 1 && adminAmount <= 1 {
				log.Error("One admin must remain, hence admin downgrade cannot be executed. Current Number of admins: %d", adminAmount)
				c.String(406, "No downgrade allowed. One user must remain admin. Please update other user first.")
			}

		} else {
			log.Info("no admin-rights detected")
			c.String(403, "no admin rights")
		}

		log.Info("changes sucessfull")
		c.Status(201)
	}
}

//deleteUser löscht User in Hashes und löscht personenbezogenen Daten aus user_users außer Global ID und Name
func deleteUser(c *gin.Context) {

	if UserIsNotLoggedIn == GetUserState(c) {
		c.String(401, "You are not logged in")
		c.Abort()
		return
	}

	log := m.Log("deleteUser")

	var (
		dataRequestorAdminflag int
		mail                   string // Mail um Hashes zu löschen
		adminAmount            int
		permission             bool
	)
	globalID, _ := strconv.Atoi(c.Param("user_id"))
	requestor := GetUserIDFromContext(c)

	//Hole Email des zu löschenden Nutzers aus Datenbank
	error := m.GetDatabase().QueryRow("SELECT mail FROM users_users WHERE user_id =?", globalID).Scan(&mail)
	if error != nil {
		log.Error("Error when fetching mail out of database, please not this error: %s", error)
		c.AbortWithError(500, error)
		return
	}
	// Hole admin_flag des Auftraggebenden Nutzers aus Datenbank
	error = m.GetDatabase().QueryRow("SELECT admin_flag FROM users_users WHERE user_id = ?", requestor).Scan(&dataRequestorAdminflag)
	if error != nil {
		log.Error("Error when fetching admin_flag of person who requested userchange")
		c.AbortWithError(500, error)
		return
	}

	// Hole Anzahl der existierenden Admins
	error = m.GetDatabase().QueryRow("Select count(admin_flag) FROM users_users WHERE admin_flag = 2").Scan(&adminAmount)
	if error != nil {
		log.Error("Error when fetching amount of admins out of user_users. Error is: %s, adminAmount: %d (-1 when initiated)", error, adminAmount)
		c.AbortWithError(500, error)
		return
	}

	log.Info("fetched data from database successfully...")

	//Abfragen ob Rechte zum löschen vorhanden sind und ob löschen legitim ist

	if requestor != globalID && dataRequestorAdminflag == 2 {
		permission = true
	} else if adminAmount > 1 && dataRequestorAdminflag == 2 {
		permission = true

		// Auftraggeber ist kein Admin darf sich also selber löschen
	} else if dataRequestorAdminflag == 1 && globalID == requestor {
		permission = true

		//Sonst keine Rechte zum löschen
	} else {
		permission = false
		log.Error("Deletion is not possible. Your status: %d (1: no admin, 2 you are admin), amount of admin: %d, your are: %d and trying to delete: %d", dataRequestorAdminflag, adminAmount, requestor, globalID)
		c.String(403, "No Permission")
		c.Abort()
		return
	}

	if permission {

		log.Info("Congrats, you got the Rights to delete! Well then... deleting user: %s in hashes", mail)
		//delete user in hashes
		for key, value := range hashes {
			if value == mail {
				delete(hashes, key)
			}
		}

		log.Info("user: %s successfully in hashes deleted, now deleting in database", mail)
		// update datenbank, lösche alle daten, außer Global ID und Name
		// Global ID gibt nur Name zurück
		_, err := m.GetDatabase().Exec("UPDATE users_users SET mail = NULL, password = NULL, admin_flag = 0, change_password= 0 WHERE user_id = ?", globalID)
		if err != nil {
			log.Error("Error when deleting user. Error is: %s", err)
			c.AbortWithError(500, err)
			return
		}

		log.Info("Deleted - goodbye then")
		c.Status(200)
	}
}
