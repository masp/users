-- +goose Up
-- Database for Usermodule

CREATE TABLE
IF NOT EXISTS users_users
(
    user_id INT NOT NULL PRIMARY KEY,
    --- Global ID
    name TEXT NOT NULL,
    --- Echter Name
    mail TEXT UNIQUE,
    password TEXT,
    admin_flag INT,
    --- 1 -> Normaler Nutzer; 2 -> Projektadmin
    change_password INT,
    --- 0 -> Password muss nicht geändert werden; 1 -> Password muss beim nächsten Login geändert werden
     FOREIGN KEY
(user_id) REFERENCES base_global_ids
(global_id) ON
DELETE CASCADE
);

