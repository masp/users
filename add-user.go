package users

import (
	"database/sql"
	"strings"

	"golang.org/x/crypto/bcrypt"

	"github.com/gin-gonic/gin"
	"github.com/pengux/check"
)

// Handler Funktion für das hinzufügen eines neuen Nutzers über die Rest-API
func addUser(c *gin.Context) {
	user := GetUserState(c)
	if user == UserIsNotLoggedIn {
		c.AbortWithStatus(401)
		return
	}
	if user == UserIsLoggedIn {
		c.AbortWithStatus(403)
		return
	}
	l := m.Log("Add User")
	l.Info("Function for creating a new user was called")
	saveUser := User{}
	err := c.BindJSON(&saveUser)
	if err != nil {
		l.Error("Error while binding json: %s", err)
		c.AbortWithError(500, err)
		return
	}
	if saveUser.Password != "" {
		l.Error("User tried to set initial password")
		c.String(400, "You are not allowed to set a Password for a new User")
		return
	}

	s := check.Struct{
		"Name": check.Composite{
			check.NonEmpty{},
			check.MinChar{2},
		},
		"Mail": check.Composite{
			check.NonEmpty{},
			check.Email{},
		},
		"AdminFlag": check.Composite{
			check.NonEmpty{},
			check.LowerThan{3},
			check.GreaterThan{0},
		},
	}
	e := s.Validate(&saveUser)
	if e.HasErrors() {
		str := ""
		for k, x := range e.ToMessages() {
			for _, y := range x {
				str = str + "\n" + k + " - " + y
				l.Error("Validation Error: %s - %s", k, y)
			}
		}

		c.String(400, str)
		return
	}
	saveUser.Mail = strings.TrimSpace(saveUser.Mail)
	saveUser.Name = strings.TrimSpace(saveUser.Name)
	l.Info("User succesful unmarshaled: user: %s, mail: %s, admin flag: %d", saveUser.Name, saveUser.Mail, saveUser.AdminFlag)
	var dbMail string
	err = m.GetDatabase().QueryRow("SELECT mail FROM users_users WHERE mail = ?", saveUser.Mail).Scan(&dbMail)
	if err != sql.ErrNoRows {
		if dbMail == saveUser.Mail {
			l.Info("Mail already in DB, aboard user creation")
			c.AbortWithStatus(409)
			return
		}
	}
	if err != nil && err != sql.ErrNoRows {
		l.Error("Database querry error: %s", err)
		c.AbortWithStatus(500)
		return
	}
	// Passwort generieren
	saveUser.Password = randomString(12)
	hash, err := bcrypt.GenerateFromPassword([]byte(saveUser.Password), bcrypt.DefaultCost)
	if err != nil {
		l.Error("Hash error: %s", err)
		c.AbortWithStatus(500)
	}
	m.GetDatabase().Exec("INSERT INTO users_users (user_id, name, password, mail, admin_flag, change_password ) VALUES (?, ?, ?, ?, ?, ?)", m.GetNewGlobalID(), saveUser.Name, string(hash), saveUser.Mail, saveUser.AdminFlag, 1)

	if m.MailDisabled() {
		l.Info("Mailing is disabled")
		c.JSON(201, gin.H{
			"mail":     saveUser.Mail,
			"password": saveUser.Password,
			"name":     saveUser.Name,
			"globalID": saveUser.UserID,
		})
		l.Debug("Usercreation succesfull")
		return
	}
	err = m.SendMail(map[string]string{saveUser.Mail: saveUser.Name}, "newUser", map[string]interface{}{"Subject": "Willkommen bei masp", "Name": saveUser.Name, "Link": c.Request.Host, "Mail": saveUser.Mail, "Password": saveUser.Password})
	if err != nil {
		l.Error("An error occured while sending a Mail: %s", err)
		m.GetDatabase().Exec("DELETE FROM users_users WHERE mail = ?", saveUser.Mail)
		c.AbortWithStatus(500)
		return
	}
	l.Info("Mail was send succesful.")
	l.Debug("Usercreation succesfull")
	//Gin gibt zurück, dass der Nutzer eine Mail bekommen hat mit seinen Login Daten
	c.JSON(201, gin.H{
		"redirect": true,
	})
}
