package users

import (
	"os"
	"path"
	"runtime"

	"git.mo-mar.de/masp/masp"
	"github.com/coreos/go-semver/semver"
	"github.com/gin-gonic/gin"
)

// Module definiert das Modul
type Module struct{}

// M ist die Instanz des Moduls und muss immer so heißen, damit andere Module darauf zugreifen können
var M = Module{}

// m referenziert die API des Basismoduls und ist private, damit kein Modul sich als ein anderes ausgeben kann
var m = masp.RegisterModule(&M)

// Info gibt Informationen über das Modul aus (Name, ID, Version, Verzeichnis)
func (*Module) Info() (string, string, *semver.Version, string) {
	_, filename, _, _ := runtime.Caller(0)
	return "User Accounts", "users", semver.New("1.0.0"), path.Dir(filename)
}

// Routes richtet die vom Modul benötigten Routen ein
func (*Module) Routes(router *gin.RouterGroup) {
	masp.RequireAdmin = RequireAdmin

	// router.GET|POST|...
	router.GET("/user", getUserList)
	router.GET("/user/:user_id", getUserInfo)   //@Alexa
	router.POST("/user", addUser)               //@Phillipp - add-user.go
	router.PUT("/user/:user_id", changeUser)    //@Alexa - manipulate-user.go
	router.DELETE("/user/:user_id", deleteUser) //@Alexa - manipulate-user.go
	router.POST("/login", login)                //@Phillipp - login.go
	router.GET("/logout", logout)               //@Phillipp - login.go
	router.POST("/token-login", tokenLogin)

	// Create first user
	firstUser()

	_, isSet := os.LookupEnv("MASP_TOKEN")
	if !isSet {
		l := m.Log("Startup")
		l.Error("MASP Link Token is not defined correctly. Please read documentation on masp.zottelchin.de")
		l.Warn("Now Shutting down...")
		os.Exit(0)
	}

}
