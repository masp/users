{{ .Subject}}

Hallo {{ .Name }}

Du wurdest einem masp Projekt hinzugefügt! Melde dich unter {{ .Link }} an.

{{ .Mail }}
{{ .Password }}

Wir würden Dich bitten dein Passwort nach der Anmeldung zu ändern.

Mit freundlichen Grüßen
Das MASP Team
