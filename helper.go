package users

import (
	"math/rand"
	"time"
)

// randomString generiert einen zufälligen String der Länge l
func randomString(l int) string {
	pool := "qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM<>.,;-_:()0123456789"
	rand.Seed(time.Now().UnixNano())
	s := ""
	for i := 0; i < l; i++ {
		s += string(pool[rand.Intn(len(pool))])
	}
	return s
}

// UserRole gibt den Typ des Nutzers an
type UserRole int

const (
	//UserIsNotLoggedIn -> der Nutzer ist nicht angemeldet
	UserIsNotLoggedIn = 0
	//UserIsLoggedIn -> der Nutzer ist angemeldet
	UserIsLoggedIn = 1
	//UserIsAdmin -> der Nutzer ist angemdeldet und hat Admin Privilegien
	UserIsAdmin = 2
)

// User beschreibt, wie ein Nutzer in JSON und in der Datenbank aussieht
type User struct {
	UserID      int    `json:"user_id"`
	Name        string `json:"name"`
	Password    string `json:"password,omitempty"`
	NewPassword string `json:"newpassword,omitempty"`
	Mail        string `json:"mail"`
	AdminFlag   int    `json:"admin_flag"`
}
