package users

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

//getUserInfo gibt User informationen aus und erweitert das Struct um die UserID des entsprechenden Users
func getUserInfo(c *gin.Context) {
	log := m.Log("Get UserInfo")

	// Anfrage für Task ID
	forID, _ := strconv.Atoi(c.Query("for"))

	idString := c.Param("user_id")
	if idString == "" {
		c.AbortWithStatus(400)
		log.Error("c.param id is empty")
		return
	}

	var userInfo User
	if idString == "me" {
		userInfo.UserID = GetUserIDFromContext(c)
	} else {
		userInfo.UserID, _ = strconv.Atoi(c.Param("user_id"))
	}

	if !HasLinkAccess(c, forID) && !HasLinkAccess(c, userInfo.UserID) && GetUserState(c) == UserIsNotLoggedIn {
		c.AbortWithStatus(401)
		return
	}

	err := m.GetDatabase().QueryRow("Select name, mail, admin_flag From users_users Where user_id= ?", userInfo.UserID).Scan(&userInfo.Name, &userInfo.Mail, &userInfo.AdminFlag)
	if userInfo.Mail == "" {
		log.Error("requested user has been deleted")
		c.String(404, "sorry user does not exist")
		return
	}

	if err != nil {
		log.Error("something went wrong when fetching data of user: %d out of database. error is: %s", userInfo.UserID, err)
		c.AbortWithStatus(500)
		return
	}
	log.Info("successfull...")
	c.JSON(200, userInfo)
}
