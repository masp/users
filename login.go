package users

import (
	"database/sql"
	"strings"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

//Hash Map: Hash -> Mail
//Wird als in Memory speicher für die Authentifizierungscodes genutzt
var hashes = map[string]string{}

//login: Diese Methode ist ein Handler für GET /api/users/login
// - Sie ist dafür gedacht, um den Nutzer zu authentifizieren und einen Hash zu generieren, mit dem der Nutzer sich bei weiteren Anfragen authentifizieren kann.
// - Request: Im Body werden Nutzername (Mail) und Passwort übergeben
// - Response: 200 + Hash als String, sonst Error Code
func login(c *gin.Context) {
	//TODO: password ändern erzwingen
	l := m.Log("Login")
	credentials := new(User)
	err := c.BindJSON(&credentials)
	l.Info("Login attempt for %s", credentials.Mail)
	if err != nil {
		l.Error("Json Bind error: %s", err)
		c.AbortWithError(500, err)
		return
	}
	if credentials.Mail == "" || credentials.Password == "" {
		l.Info("Mail or Password empty")
		c.AbortWithStatus(401)
		return
	}
	l.Debug("Got credentials: user: %s", credentials.Mail)
	var password []byte
	err = m.GetDatabase().QueryRow("SELECT password FROM users_users WHERE mail like ?", credentials.Mail).Scan(&password)
	if err == sql.ErrNoRows {
		l.Info("User %s not registered", credentials.Mail)
		c.AbortWithStatus(401)
		return
	}
	if err != nil {
		l.Error("SQL querry error: %s", err)
		c.AbortWithStatus(500)
		return
	}
	l.Debug("User %s is in database", credentials.Mail)
	err = bcrypt.CompareHashAndPassword(password, []byte(credentials.Password))
	if err != nil {
		l.Info("User %s tried to login with false password", credentials.Mail)
		c.AbortWithStatus(401)
		return
	}

	// --------
	// Nutzer ist authentifiziert
	// --------
	l.Info("%s is logged in", credentials.Mail)
	newHash := ""
	for {
		newHash = randomString(55)
		if hashes[newHash] == "" {
			hashes[newHash] = credentials.Mail
			l.Warn(newHash)
			c.JSON(200,
				gin.H{
					"hash": newHash,
				})
			return
		}
	}
}

//logout: Diese Methode ist ein Handler für GET /api/users/login
// - Sie ist dafür gedacht, den Nutzer von seinem aktuellen/allen Gerät(-en) abzumelden, also die Hashes aus der Hashmap zu löschen
// - Request: Den authentifizierungs Hash als Authoration Bearer Header und optional `?all=1` in der URL
// - Response: 400 - Not loged in; 202 - Erfolgreich gelöscht
func logout(c *gin.Context) {
	l := m.Log("logout")
	if GetUserState(c) == UserIsNotLoggedIn {
		c.String(401, "Your not logged in! Please login to log out")
		return
	}
	hash := c.GetHeader("Authorization")
	hash = strings.TrimPrefix(hash, "Bearer ")
	l.Info("User %s is logging out...", hashes[hash])
	if c.Query("all") == "1" {
		l.Info("... of all devices")
		user := hashes[hash]
		count := 0
		for key, value := range hashes {
			if value == user {
				count++
				delete(hashes, key)
			}
		}
		l.Debug("%d Keys deleted for %s", count, user)
	} else {
		delete(hashes, hash)
	}
	l.Info("...successful")
	c.Status(202)

}
