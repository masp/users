package users

import (
	"github.com/gin-gonic/gin"
)

//getUserList gibt eine Liste der in der Datenbank vorhanden User aus
func getUserList(c *gin.Context) {
	_, module, _, _ := M.Info()
	if GetUserState(c) == UserIsNotLoggedIn && !HasLinkAccess(c, m.GetPrefixByModule(module)) {
		c.AbortWithStatus(401)
		return
	}
	l := m.Log("Get Userlist")
	res := []User{}
	rows, err := m.GetDatabase().Query("SELECT user_id, name, mail, admin_flag FROM users_users")
	if err != nil {
		l.Error("Error while querrying database: %s", err)
		c.AbortWithStatus(500)
		return
	}
	defer rows.Close()

	for rows.Next() {
		tmp := User{}
		err := rows.Scan(&tmp.UserID, &tmp.Name, &tmp.Mail, &tmp.AdminFlag)
		if err != nil {
			if tmp.Mail == "" {
				l.Debug("Ignoring deleted User %d", tmp.UserID)
				continue
			}
			l.Error("Scanning Row error: %s", err)
		}
		l.Debug("User from Row: %v", tmp)

		res = append(res, tmp)
	}
	c.JSON(200, res)
}
