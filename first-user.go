package users

import (
	"os"

	"github.com/pengux/check"

	"golang.org/x/crypto/bcrypt"
)

//firstUser erstellt den Ersten Nutzer der Datenbank, wenn noch keiner existiert
func firstUser() {
	l := m.Log("First User")
	// Zähle die Anzahl der Nutzer in der Datenbank
	count := 0
	m.GetDatabase().QueryRow("Select count(user_id) from users_users").Scan(&count)

	// Wenn Nutzer in der Datenbank und nicht Development
	if count != 0 {
		l.Info("There are already %d users in the Database", count)
		l.Info("Starting normaly")
		return
	}

	l.Info("There is no user in the Database. Now trying to create first User")
	mail := os.Getenv("MASP_FIRST_MAIL")
	password := os.Getenv("MASP_FIRST_PASSWORD")
	if password == "" {
		password = randomString(8)
		l.Warn("There was no password set for the first user. We will create one for you.")
		l.Info("Your login will be: %s - %s", mail, password)
	}
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	ok := true
	err := check.Email{}.Validate(mail)
	if err != nil {
		l.Error("Mail %s is not a valid mail", mail)
		ok = false
	}
	err = check.NonEmpty{}.Validate(mail)
	if err != nil {
		l.Error("Mail cannot be empty")
		ok = false
	}
	err = check.NonEmpty{}.Validate(password)
	if err != nil {
		l.Error("Password cannot be empty")
		ok = false
	}
	err = check.MinChar{2}.Validate(password)
	if err != nil {
		l.Error("Password %s is to short", password)
		ok = false
	}

	if !ok {
		l.Error("First User is not defined correctly. Please read documentation on masp.zottelchin.de")
		l.Warn("Now Shutting down...")
		os.Exit(0)
	}
	m.GetDatabase().Exec("INSERT INTO users_users (user_id, name, password, mail, admin_flag, change_password ) VALUES (?, ?, ?, ?, ?, ?)", m.GetNewGlobalID(), "Erster Nutzer", string(hash), mail, 2, 1)
	l.Info("First User %s created", mail)
}
