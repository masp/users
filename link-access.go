package users

import (
	"crypto/sha1"
	"encoding/hex"
	"os"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// HasLinkAccess gibt true zurück, wenn der Nutzer für die ID Leserechte hat
func HasLinkAccess(c *gin.Context, id int) bool {
	l := m.Log("Token Access")
	l.Info("Test for Link Access")
	hash := c.GetHeader("Authorization")
	if !strings.HasPrefix(hash, "Bearer") {
		return false
	}
	hash = strings.TrimPrefix(hash, "Bearer ")
	if hash == "" {
		return false
	}
	hashAccessID, ok := linkAccess[hash]
	if ok && id == hashAccessID {
		return true
	}
	return false
}

// GenerateLink generiert einen ReadOnly Link für das Objekt mit der Global ID gid
func GenerateLink(c *gin.Context, gid int) string {
	hash := sha1.Sum([]byte(os.Getenv("MASP_TOKEN") + strconv.Itoa(gid)))
	token := hex.EncodeToString(hash[:]) + strconv.Itoa(gid)
	return "/@" + token
}

// tokenLogin prüft den Token und gibt einen Hash zur identifizierung zurück
func tokenLogin(c *gin.Context) {
	l := m.Log("token login")
	token := os.Getenv("MASP_TOKEN")

	json := struct {
		Token string `json:"token"`
	}{}
	c.BindJSON(&json)

	hash := strings.ToLower(json.Token[:40])
	id := json.Token[40:]
	tmp := (sha1.Sum([]byte(token + id)))
	checksum := hex.EncodeToString(tmp[:])

	idInt, _ := strconv.Atoi(id)
	if checksum == hash {
		l.Info("Access allowed")
		for {
			newHash := randomString(55)
			if linkAccess[newHash] == 0 {
				linkAccess[newHash] = idInt
				c.JSON(200,
					gin.H{
						"hash": newHash,
					})
				return
			}
		}
	}
	l.Info("Access denied")
	c.String(401, "Token isn't correct.")
}

//Hashmap für LinkAccess: hash -> global ID
var linkAccess = map[string]int{}
